# ADV processing framework
Everglades Systems Assesment Group at the **South Florida Water Management District**

## Prerequisites
* `R`
* `git`
* `zoo` R package

## Installation
Building from source using the command line:

`$ git clone git@gitlab.com:jsta/adv.git`

`$ R CMD build adv`

`$ R CMD INSTALL adv_0.1.tar.gz`

## Example

`library(adv)`

`example("spikegornik")`

